package devsmonster;

import java.util.HashMap;
import java.util.Map;

public class Type {
	
	private	int id;
	private	String nom;
	private String couleur;
	public static Map<Integer, Type> listeType = new HashMap<>();
	private Map<Integer, Affinite> listeAffinites = new HashMap<>();
	
	public Type(int id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public Type(int id, String nom, String couleur) {
		this.id = id;
		this.nom = nom;
		this.couleur = couleur;
	}
	
	public Type(int id, String nom, Map<Integer, Affinite> listeAffinites) {
		this.id = id;
		this.nom = nom;
		this.listeAffinites = listeAffinites;
	}
	
	public Type(int id, String nom, String couleur, Map<Integer, Affinite> listeAffinites) {
		this.id = id;
		this.nom = nom;
		this.couleur = couleur;
		this.listeAffinites = listeAffinites;
	}
	
	public String toString() {
		return getNom();
	}

	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getCouleur() {
		return couleur;
	}
	
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public Map<Integer, Affinite> getListeAffinites() {
		return listeAffinites;
	}
	
	public void setListeAffinites(Map<Integer, Affinite> listeAffinites) {
		this.listeAffinites = listeAffinites;
	}
	
	public void updateTypeBdd() {
		BDD.updateType(id, nom);
	}
	
	public static void resume() {
		System.out.println("\nListe des types : ");
		for (Map.Entry<Integer, Type> type : Type.listeType.entrySet()) {
		    System.out.println("\n- id = " + type.getKey() + " / nom = " + type.getValue().getNom());
		    System.out.println("- Liste des affinites : ");
		    for (Map.Entry<Integer, Affinite> affinite : type.getValue().getListeAffinites().entrySet()) {
			    System.out.println("-- id = " + affinite.getKey() + " / nom = " + (String)Type.listeType.get(affinite.getKey()).getNom()  + " / coef" + affinite.getValue().getCoef());
			}
		}
	}
}
