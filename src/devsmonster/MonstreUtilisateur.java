package devsmonster;

public class MonstreUtilisateur {
	private int idMonstreUtilisateur;
	private String nom;
	private int idMonstre;
	private int idUtilisateur;
	private int experience;

	private int pv;
	
	public MonstreUtilisateur(int idMonstreUtilisateur, String nom, int idMonstre, int idUtilisateur, int experience) {
		this.idMonstre = idMonstre;
		this.nom = nom;
		this.idMonstreUtilisateur = idMonstreUtilisateur;
		this.idUtilisateur = idUtilisateur;
		this.experience = experience;
		this.pv = this.getMonster().getPv();
	}
	
	public int getIdMonstre() {
		return idMonstre;
	}
	
	public String getNom() {
		if(!nom.isEmpty()) {
			return nom;
		}
		else {
			return this.getMonster().getNom();
		}
	}
	
	public int getIdMonstreUtilisateur() {
		return idMonstreUtilisateur;
	}
	
	public int getIdUtilisateur() {
		return idUtilisateur;
	}
	
	public int getExperience() {
		return experience;
	}
	
	public int getNiveau() {
		//int niv = this.experience^(1/3);
		int niv = 50;
		return niv;
	}
	
	private int getStat(int base) {
		int stat = ((2 * base * this.getNiveau())/100)+5;
		return stat;
	}
	
	public int getAttaque() {
		int att = this.getStat(Monstre.listeMonstre.get(idMonstre).getAttaque());
		return att;
	}
	
	public int getDefense() {
		int def = this.getStat(Monstre.listeMonstre.get(idMonstre).getDefense());
		return def;
	}
	
	public int getVitesse() {
		int spd = this.getStat(Monstre.listeMonstre.get(idMonstre).getVitesse());
		return spd;
	}

	public int getPv() {
		return pv;
	}
	
	public void setPv(int pv) {
		this.pv = pv;
	}
	
	public Monstre getMonster() {
		return Monstre.listeMonstre.get(this.idMonstre);
	}
	
}
