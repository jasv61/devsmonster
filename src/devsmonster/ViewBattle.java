package devsmonster;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class ViewBattle extends JPanel {              

	private Battle battle = new Battle();
	
	private JPanel panelCombat = new JPanel();
	private JPanel panelEquipe = new JPanel();
	private JPanel panelJ1 = new JPanel();
	private JPanel panelJ2 = new JPanel();
	private JLabel lblImageMonstre1 = new JLabel("");
	private JLabel lblImageMonstre2 = new JLabel("");
	private JLabel nomMonstre1 = new JLabel("");
	private JLabel nomMonstre2 = new JLabel("");
	private JProgressBar barreVieMonstre1 = new JProgressBar();
	private JProgressBar barreVieMonstre2 = new JProgressBar();
	private JLabel lblPvMonstre1 = new JLabel("");
	private JLabel lblPvMonstre2 = new JLabel("");
	private JPanel panelAction = new JPanel();
	private JPanel panelAttaque = new JPanel();
	private JButton[] btnAttaques = new JButton[4];
	private JPanel panelOption = new JPanel();
	private JLabel nomJoueur = new JLabel("");
	private JButton btnChanger = new JButton("Changer monstre");
	private JButton btnQuitter = new JButton("Quitter");
	private JLabel nomJoueurEquipe = new JLabel("");
	private JButton btnRetourEquipe = new JButton("Retour");
	private JButton[] btnMonstreEquipe = new JButton[6];
	
	public ViewBattle() {
		setBackground(Color.DARK_GRAY);
		
		initialize();			
	}

	private void initialize() {		
		
		setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[100%]"));
		
		panelCombat.setBackground(Color.DARK_GRAY);
		panelCombat.setLayout(new MigLayout("", "[50%,grow][50%,grow]", "[40%,grow][40%,grow][20%,grow]"));
		add(panelCombat, "cell 0 0,grow");
		
		panelEquipe.setBackground(Color.DARK_GRAY);
		panelEquipe.setLayout(new MigLayout("", "[50%][50%]", "[20%][20%][20%][20%][20%]"));
		
		panelJ1.setOpaque(false);
		panelCombat.add(panelJ1, "cell 1 1,grow");
		panelJ1.setLayout(new MigLayout("", "[100%]", "[33.33%][33.33%][33.33%]"));
		
		panelJ2.setOpaque(false);
		panelCombat.add(panelJ2, "cell 0 0,grow");
		panelJ2.setLayout(new MigLayout("", "[100%]", "[33.33%][33.33%][33.33%]"));
		
		panelCombat.add(lblImageMonstre1, "cell 0 1,alignx center");
		
		panelCombat.add(lblImageMonstre2, "cell 1 0,alignx center,aligny center");
		
		nomMonstre1.setForeground(Color.WHITE);
		panelJ1.add(nomMonstre1, "cell 0 0,alignx center,aligny bottom");
		
		nomMonstre2.setForeground(Color.WHITE);
		panelJ2.add(nomMonstre2, "cell 0 0,alignx center,aligny bottom");
		
		panelJ1.add(barreVieMonstre1, "cell 0 1,growx");
		
		panelJ2.add(barreVieMonstre2, "cell 0 1,growx");	
		
		lblPvMonstre1.setForeground(Color.WHITE);
		panelJ1.add(lblPvMonstre1, "cell 0 2,alignx center,aligny top");
		
		lblPvMonstre2.setForeground(Color.WHITE);
		panelJ2.add(lblPvMonstre2, "cell 0 2,alignx center,aligny top");
		
		panelAction.setBackground(Color.GRAY);
		panelCombat.add(panelAction, "cell 0 2 2 1,grow");
		panelAction.setLayout(new MigLayout("", "[70%,grow][30%,grow]", "[100%,grow]"));
		
		panelAttaque.setOpaque(false);
		panelAction.add(panelAttaque, "cell 0 0,grow");
		panelAttaque.setLayout(new MigLayout("", "[50%][50%]", "[50%][50%]"));
		
		panelOption.setOpaque(false);
		panelAction.add(panelOption, "cell 1 0,grow");
		
		panelOption.setLayout(new MigLayout("", "[100%]", "[33.33%][33.33%][33.33%]"));
		
		nomJoueur.setForeground(Color.WHITE);
		panelOption.add(nomJoueur, "cell 0 0,alignx center,growy");
		
		btnChanger.setBackground(Color.DARK_GRAY);
		btnChanger.setForeground(Color.WHITE);
		btnChanger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(panelEquipe, "cell 0 0,grow");
				invalidate();
				validate();
				repaint();
			}
		});
		panelOption.add(btnChanger, "cell 0 1,grow");
		
		btnQuitter.setForeground(Color.WHITE);
		btnQuitter.setBackground(Color.DARK_GRAY);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModePlayer selectModePlayer = new SelectModePlayer();
				Main.fen.setContentPane(selectModePlayer);
			}
		});
		panelOption.add(btnQuitter, "cell 0 2,grow");
		
		nomJoueurEquipe.setForeground(Color.WHITE);
		
		btnRetourEquipe.setBackground(Color.GRAY);
		btnRetourEquipe.setForeground(Color.WHITE);
		btnRetourEquipe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(panelCombat, "cell 0 0,grow");
				invalidate();
				validate();
				repaint();
			}
		});
		
		actionCombat();
	}

	private void testMonstres(Utilisateur utilisateur, Utilisateur utilisateur2) {
		boolean test = true;
		for (int i = 0; i < utilisateur.getEquipeMonstre().size(); i++) {
			if(utilisateur.getEquipeMonstre().get(i).getPv() > 0) {
				test = false;
			}
		}
		
		if(test) {
			Victoire victoire = new Victoire(utilisateur2, utilisateur);
			Main.fen.setContentPane(victoire);
		}
	}
	
	private void actionCombat() {
		Utilisateur utilisateurActuel;
		Utilisateur utilisateurAdversaire;
		
		if(battle.getTour() == false) {
			utilisateurActuel = Utilisateur.utilisateur1;
			utilisateurAdversaire = Utilisateur.utilisateur2;
			
		}
		else {
			utilisateurActuel = Utilisateur.utilisateur2;
			utilisateurAdversaire = Utilisateur.utilisateur1;
		}
		
		MonstreUtilisateur monstreJ1 = utilisateurActuel.getMonstreActuel();
		MonstreUtilisateur monstreJ2 = utilisateurAdversaire.getMonstreActuel();
		
		ImageIcon imageJ1 = new ImageIcon(monstreJ1.getMonster().getImageDos());
		lblImageMonstre1.setIcon(imageJ1);
		
		ImageIcon imageJ2 = new ImageIcon(monstreJ2.getMonster().getImage());	
		lblImageMonstre2.setIcon(imageJ2);
		
		nomMonstre1.setText(monstreJ1.getNom());
		nomMonstre2.setText(monstreJ2.getNom());
		
		barreVieMonstre1.setMaximum(monstreJ1.getMonster().getPv());
		barreVieMonstre1.setValue(monstreJ1.getPv());
		
		barreVieMonstre2.setMaximum(monstreJ2.getMonster().getPv());
		barreVieMonstre2.setValue(monstreJ2.getPv());
		
		lblPvMonstre1.setText(monstreJ1.getPv() + " / " + monstreJ1.getMonster().getPv());
		lblPvMonstre2.setText(monstreJ2.getPv() + " / " + monstreJ2.getMonster().getPv());
		
		panelAttaque.removeAll();
		
		int x = 0, y = 0;
		for(int i = 0; i < utilisateurActuel.getMonstreActuel().getMonster().getCapacites().length; i++) {
			int il = i;
			btnAttaques[i] = new JButton(utilisateurActuel.getMonstreActuel().getMonster().getCapacite(i).getNom());
			if(!utilisateurActuel.getMonstreActuel().getMonster().getCapacite(i).getType().getCouleur().isEmpty()) {
				Color couleur = Color.decode(utilisateurActuel.getMonstreActuel().getMonster().getCapacite(i).getType().getCouleur());
				btnAttaques[i].setForeground(couleur);
				btnAttaques[i].setBorder(BorderFactory.createLineBorder(couleur));
			}
			else {
				btnAttaques[i].setForeground(Color.WHITE);
			}
			btnAttaques[i].setBackground(Color.DARK_GRAY);
			
			btnAttaques[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						vieAnimation(monstreJ1, monstreJ2, il);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					testMonstres(utilisateurAdversaire, utilisateurActuel);
					actionCombat();
				}
			});
			panelAttaque.add(btnAttaques[i], "cell " + x + " " + y + ",grow");
			if(x == 1) {
				x = 0;
				y++;
			}
			else {
				x++;
			}
		}
		
		nomJoueur.setText(utilisateurActuel.getNom());
		
		nomJoueurEquipe.setText(utilisateurActuel.getNom());
		
		//Page changement monstre 
		x = 0;
		y = 1;
		
		panelEquipe.removeAll();
		btnRetourEquipe.setEnabled(true);
		panelEquipe.add(btnRetourEquipe, "cell 0 4 2 1,alignx center,aligny center");
		panelEquipe.add(nomJoueurEquipe, "cell 0 0 2 1,alignx center,aligny center");
		for (int i = 0; i < utilisateurActuel.getEquipeMonstre().size(); i++) {
			int il = i;
			
			btnMonstreEquipe[i] = new JButton(utilisateurActuel.getEquipeMonstre().get(i).getNom());
			if(!utilisateurActuel.getEquipeMonstre().get(i).getMonster().getType().getCouleur().isEmpty()) {
				Color couleurBtnEquipeMonstre = Color.decode(utilisateurActuel.getEquipeMonstre().get(i).getMonster().getType().getCouleur());
				btnMonstreEquipe[i].setBorder(BorderFactory.createLineBorder(couleurBtnEquipeMonstre));
			}
			btnMonstreEquipe[i].setBackground(Color.GRAY);
			btnMonstreEquipe[i].setForeground(Color.WHITE);
			if(utilisateurActuel.getEquipeMonstre().get(i).getPv() <= 0) {
				btnMonstreEquipe[i].setEnabled(false);
			}
			btnMonstreEquipe[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					utilisateurActuel.setIdMonstreActuel(il);
					actionCombat();
					removeAll();
					add(panelCombat, "cell 0 0,grow");
					invalidate();
					validate();
					repaint();
				}
			});
			
			String align;
			if(x == 0) {
				align = "right";
			}
			else {
				align = "left";
			}
			
			panelEquipe.add(btnMonstreEquipe[i], "cell "+ x +" " + y + ",alignx "+ align +",aligny center");
			
			if(x == 1) {
				x = 0;
				y++;
			}
			else {
				x++;
			}
			
			
		}	
		if(battle.getTour() == false) {
			battle.setTour(true);
		}
		else {
			battle.setTour(false);
		}
		
		if(monstreJ1.getPv() <= 0) {
			removeAll();
			btnRetourEquipe.setEnabled(false);
			add(panelEquipe, "cell 0 0,grow");
			invalidate();
			validate();
			repaint();
		}
	}
	
	public void vieAnimation(MonstreUtilisateur monstre1, MonstreUtilisateur monstre2, int idAttaque) throws InterruptedException {
		int pvPerdu = battle.attaque(monstre1, monstre2, idAttaque);
		
		for(int i = 0; i < pvPerdu && monstre2.getPv()-i >=0 ; i++) {
			barreVieMonstre2.setValue(monstre2.getPv()-i);
			Rectangle progressRect = barreVieMonstre2.getBounds();
			progressRect.x = 0;  
            progressRect.y = 0;  
            barreVieMonstre2.paintImmediately(progressRect);  
			Thread.sleep(50);
		}
		monstre2.setPv(monstre2.getPv()-pvPerdu);
	}
}