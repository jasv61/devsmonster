package devsmonster;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class BDD {
	
	 private static String bdurl = "jdbc:mysql://localhost/bd_sio";  
	 private static String bdlogin = "root";
	 private static String bdpasswd = "";
	 private static Connection cn = null;
	 private static Statement st = null;
	 private static ResultSet rs = null;

	 public static void initialisationBDD() {
		try {
			//   chargement du driver
			Class.forName("com.mysql.jdbc.Driver");
			//   recuperation de la connexion
			cn = (Connection) DriverManager.getConnection(bdurl, bdlogin, bdpasswd);
			st = (Statement) cn.createStatement();
			System.out.println("Connexion a la BDD reussie");
			} 
		catch (SQLException e) {
			JOptionPane.showConfirmDialog(null, "Ouverture Impossible de la Base de Donnees (e)",
					"Dev Monster", JOptionPane.PLAIN_MESSAGE);
			 e.printStackTrace();
			 System.exit(0);
		} 
		catch (ClassNotFoundException f) {
		   JOptionPane.showConfirmDialog(null, "Ouverture Impossible de la Base de Donnees (f)", 
				   "Dev Monster", JOptionPane.PLAIN_MESSAGE); 
		   f.printStackTrace();
		   System.exit(0);
		}
	}
	
	public  Connection getCn() {
		return cn;
	}
	
	public String getUrl() {
		return bdurl;
	}
	
	// Utilisateur
	public static ArrayList<Utilisateur> getUtilisateurs() {
		ArrayList<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		try {
			rs = st.executeQuery("Select * from user");
			if (!rs.next()) {
				System.out.print("Aucun utilisateur touve !");
			}
			else {
				do{
					int id =rs.getInt("usr_id");
					String nom =rs.getString("usr_login");
					String motDePasse =rs.getString("usr_password");
					listeUtilisateurs.add( new Utilisateur ( id , nom, motDePasse)) ;
				}
				while(rs.next());
			}
		} 
		catch (SQLException e) {
			System.out.println("Erreur liee a la recuperation des utilisateurs en BDD !");
		}
		rs = null;
		return listeUtilisateurs;
	}
	
	public static int setUtilisateur(String login, String password) {
		int id = 0;
		try {
			String requete ="INSERT INTO user (usr_login, usr_password)  VALUES ("
					+ " '"+login +"'," + " '"+password + "'" + ")";
			id = getInsertId(requete);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'utilisateur " + login + 
					" existe deja", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
			}
			e.printStackTrace();
		}
		return id;
	}
	
	public static boolean updateUtilisateur(int id, String login, String password) {
		try {
			st.executeUpdate("UPDATE user SET usr_login = '" + login + "', usr_password = '" + password 
					+ "' WHERE usr_id = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'utilisateur " + login + 
						" n'a pas ete mis a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteUtilisateur(int id) {
		try {
			st.executeUpdate("DELETE FROM user WHERE usr_id = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'utilisateur n'a pas �t� supprim�", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static int testUtilisateur(String login, String password) {
		try {
	        String sql = "SELECT * FROM user where usr_login = '" + login +"' and usr_password = '"+ password +"'" ;
	        rs = st.executeQuery(sql);
	        rs.next();
	        if((rs.getString(2) != " ") && (rs.getString(3)!= " ") ) {
	            return rs.getInt(1);	
	        }
	    } 
	    catch (SQLException e) {
	    e.printStackTrace();
	    } 
		rs = null;
	    return 0;
	}
	
	// Type
	public static Map<Integer, Type> getTypes() {
		Map<Integer, Type> listeTypes = new HashMap<>();
		
		try {
			rs = st.executeQuery("Select * from type");
			
			if (!rs.next()) { 
				System.out.println("Aucun type touve !");
			}
			
			else {
				do{
					int id =rs.getInt("tpe_id");
					String nom =rs.getString("tpe_nom");
					String couleur = rs.getString("tpe_color"); 
					listeTypes.put(id, new Type (id, nom, couleur));
				}
				while(rs.next());
			}
		} catch (SQLException e) {
			System.out.println("Erreur liee a la recuperation des types en BDD !");
		}
		rs = null;
		return listeTypes;
	}
	
	public static int setType(String nom) {
		int id = 0;
		try {
			String requete = "INSERT INTO type (tpe_nom)  VALUES ('"+ nom + "')";
			id = getInsertId(requete);
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'ajout du type " + nom + 
					" n'a pas fonctionnee !", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
			}
			e.printStackTrace();
		}
		return id;
	}
	
	public static boolean updateType(int id, String nom) {
		try {
			st.executeUpdate("UPDATE type SET tpe_nom = '" + nom + "' WHERE tpe_id = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le type " + nom + 
						" n'a pas ete mis a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteType(int id) {
		try {
			st.executeUpdate("DELETE FROM type WHERE tpe_id = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le type n'a pas �t� supprim�", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	// Affinit�
	
	public static Map<Integer, Affinite> getAffinites(int idFor) {
		Map<Integer, Affinite> listeAffinitesType = new HashMap<>();
		try {
			rs = st.executeQuery("Select * from affinity where aff_tpe_for_id = " + idFor);
			if (!rs.next()) { 
				System.out.println("Aucune affinite touvee pour l'id = "+ idFor +" !");
			}
			else {
				do{
					int id = rs.getInt("aff_id");
					int idType1 = rs.getInt("aff_tpe_for_id");
					int idType2 = rs.getInt("aff_tpe_against_id");
					float coef = rs.getFloat("aff_coef");
					listeAffinitesType.put(idType1, new Affinite(id, idType1, idType2, coef));					
				}
				while(rs.next());
			}
		} 
		catch (SQLException e) {
			System.out.println("Erreur liee a la recuperation des affinites en BDD !");
		}
		rs = null;
		return listeAffinitesType;
	}
	
	public static int setAffinite(int idType1, int idType2, float coef) {
		int id = 0;
		try {
			String requete = "INSERT INTO affinity (aff_coef, aff_tpe_for_id, aff_tpe_against_id)  "
					+ "VALUES ('"+ coef + "', '" + idType1 +"', '"+ idType2 + "')";
			id = getInsertId(requete);
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'ajout de l'affinite n'a pas fonctionnee !", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
			}
			e.printStackTrace();
		}
		return id;
	}
	
	public static boolean updateAffinite(int id, int idType1, int idType2, float coef) {
		try {
			st.executeUpdate("UPDATE affinity SET aff_tpe_for_id = '" + idType1 + "', aff_tpe_against_id = '" + idType2 
					+ "', aff_coef = '"+ coef + "' WHERE aff_id = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'affinite n'a pas ete mise a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteAffinite(int id) {
		try {
			st.executeUpdate("DELETE FROM affinity WHERE aff_id = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "L'affinite n'a pas �t� supprim�e", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}

	// Monstre
	public static Map<Integer,Monstre> getMonstres() {
		Map<Integer,Monstre> listeMonstres = new HashMap<>();
		try {
			rs = st.executeQuery("Select * from monster");
			if (!rs.next()) { 
				System.out.print("Aucun monstre touve !");
			}
			else {
				do{
					int id =rs.getInt("mst_id");
					String nom =rs.getString("mst_name");
					int dommage =rs.getInt("mst_attack");
					int defense =rs.getInt("mst_defense");
					int vitesse =rs.getInt("mst_speed");
					int pv =rs.getInt("mst_life_point");
					float experience =rs.getFloat("mst_experience_coef");
					String image =rs.getString("mst_image");
					String imageDos =rs.getString("mst_image_back");
					int idType =rs.getInt("mst_type_id");
					listeMonstres.put(id, new Monstre ( id , nom, dommage, defense, vitesse, pv, experience, image, imageDos, idType)) ;
				}
				while(rs.next());
			}
		} 
		catch (SQLException e) {
			System.out.println("Erreur liee a la recuperation des monstre en BDD !");
		}
		rs = null;
		return listeMonstres;
	}
	
	public static int setMonstre(String nom, int attaque, int defense, int vitesse, int pv, int coefExperience, 
			String image, int type) {
		int id = 0;
	    try {
	        String requete = "INSERT INTO monster (mst_name, mst_attack, mst_defense, mst_speed, mst_life_point,"
        		+ " mst_experience_coef, mst_image, mst_type_id)  VALUES ("
                + " '"+ nom +"', '"+ attaque + "', '"+ defense +"', '"+ vitesse + "', '"
    			+ pv +"', '"+ coefExperience + "', '"+ image +"', '"+ type + "')";
	        
	        id = getInsertId(requete);
	    } catch (SQLException e) {
	        if(e.getErrorCode() == 1062) {
	            JOptionPane.showConfirmDialog(null, "Le monstre " + nom  +" existe deja�", "Dev Montser", 
	            		JOptionPane.PLAIN_MESSAGE);
	        }
	        e.printStackTrace();
	    }
	    return id;
	}

	public static boolean updateMonstre(int id, String nom, int attaque, int defense, int vitesse, int pv, float coefExperience, 
			String image, int type) {
		try {
			st.executeUpdate("UPDATE monster SET mst_name = '" + nom + "', mst_attack = '" + attaque + 
					"', mst_defense = '"+ defense +	"', mst_speed = '"+ vitesse + "', mst_life_point = '"+ pv 
					+ "', mst_experience_coef = '"+ coefExperience + "', mst_image = '"+ image + "', mst_type = '"+ type +
					"' WHERE mst_id = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le monstre n'a pas ete mise a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteMonstre(int id) {
		try {
			st.executeUpdate("DELETE FROM monster WHERE mst_id = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le monstre n'a pas �t� supprim�e", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	// Monstre utilisateur
	
	public static ArrayList<MonstreUtilisateur> getMonstresUtilisateur(int id) {
		ArrayList<MonstreUtilisateur> listeMonstresUtilisateur = new ArrayList<MonstreUtilisateur>();
		try {
			rs = st.executeQuery("Select * from user_monster where usr_mst_user_id = "+ id);
			if (!rs.next()) {
				System.out.print("Aucun monstre touve !");
			}
			else {
				do{
					int idMonstreUtilisateur =rs.getInt("usr_mst_id");
					String nom =rs.getString("usr_mst_name");
					int idMonstre =rs.getInt("usr_mst_monster_id");
					int idUtilisateur =rs.getInt("usr_mst_user_id");
					int experience =rs.getInt("usr_mst_experience");
					listeMonstresUtilisateur.add( new MonstreUtilisateur ( idMonstreUtilisateur , nom, idMonstre, idUtilisateur, experience)) ;
				}
				while(rs.next());
			}
		} 
		catch (SQLException e) {
			System.out.println("Erreur liee a la recuperation des monstres de l'utilisateur en BDD !");
		}
		rs = null;
		return listeMonstresUtilisateur;
	}
	
	public static int setMonstresUtilisateurs(String nom, int experience, int idUtilisateur, int idMonstre) {
		int id = 0;
	    try {
	        String requete = "INSERT INTO user_monster (usr_mst_name, usr_mst_experience, usr_mst_user_id , usr_mst_monster_id )  VALUES ("
	                + " '" + nom +"', '" + experience + "', '" + idUtilisateur + "', '"+ idMonstre + "')";
	        id = getInsertId(requete);
	    } catch (SQLException e) {
	        if(e.getErrorCode() == 1062) {
	            JOptionPane.showConfirmDialog(null, "Le monstre n'a pas ete enregistre", "Dev Montser", 
	            		JOptionPane.PLAIN_MESSAGE);
	        }
	        e.printStackTrace();
	    }
	    return id;
	}
	
	public static boolean updateMonstreUtilisateur(int id, String nom, int experience, int idUtilisateur, int idMonstre) {
		try {
			st.executeUpdate("UPDATE user_monster SET usr_mst_name = '" + nom + "', usr_mst_experience = '" + experience + 
					"', usr_mst_user_id  = '"+ idUtilisateur +	"', usr_mst_monster_id  = '"+ idMonstre + "' WHERE mst_id = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le monstre n'a pas ete mise a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteMonstreUtilisateur(int id) {
		try {
			st.executeUpdate("DELETE FROM user_monster WHERE usr_mst_id  = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "Le monstre n'a pas �t� supprim�e", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	//Capacit�
	public static Map<Integer, Capacite> getCapacites(){
		Map<Integer, Capacite> listeCapacites = new HashMap<>();
		
		try {
			rs = st.executeQuery("Select * from capacity");
			if (!rs.next()) { 
				System.out.print("Aucun capacite touvee !");
			}
			
			else {
				do{
					int id =rs.getInt("cpt_id");
					String nom =rs.getString("cpt_name");
					int dommage =rs.getInt("cpt_damage");
					String animation =rs.getString("cpt_animation");
					int idType =rs.getInt("cpt_type_id");
					listeCapacites.put(id,  new Capacite (id, nom, dommage, animation, idType)) ;
				}
				while(rs.next());
			}
		}
		
		catch (SQLException e){
			System.out.println("Erreur liee a la recuperation des capacites en BDD !");
		}
		rs = null;
		return listeCapacites;
	}
	
	public static int setCapacite(String nom, int dommage, String animation, int idType) {
		int id = 0;
	    try {
	        String requete = "INSERT INTO capacity (cpt_name, cpt_damage, cpt_animation, cpt_type_id)  VALUES ("
	                + " '" + nom +"', '" + dommage + "', '" + animation + "', '"+ idType + "')";
	        id = getInsertId(requete);
	    } catch (SQLException e) {
	        if(e.getErrorCode() == 1062) {
	            JOptionPane.showConfirmDialog(null, "La capacite " + nom  +" existe deja", "Dev Montser", 
	            		JOptionPane.PLAIN_MESSAGE);
	        }
	        e.printStackTrace();
	    }
	    return id;
	}

	public static boolean updateCapacite(int id, String nom, int dommage, String animation, int idType) {
		try {
			st.executeUpdate("UPDATE capacity SET cpt_name = '" + nom + "', cpt_damage = '" + dommage + 
					"', cpt_animation  = '"+ animation +	"', cpt_type_id   = '"+ idType + "' WHERE cpt_id  = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "La capacite n'a pas ete mise a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteCapacite(int id) {
		try {
			st.executeUpdate("DELETE FROM capacity WHERE cpt_id  = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "La capacite n'a pas �t� supprim�e", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	//Capacit� monstre
	public static int[] getCapacitesMonstre(int id){
		int[] listeCapacitesMonstre = new int[4];
		try {
			rs = st.executeQuery("Select * from monster_capacity where mst_cpt_monster_id = "+ id);
			if (!rs.next()) { 
				System.out.print("Aucun capacite monstre touvee !");
			}
			
			else {
				int i = 0;
				do{
					int idCapacite =rs.getInt("mst_cpt_capacity_id");
					listeCapacitesMonstre[i] = idCapacite ;
					i++;
				}
				while(rs.next() && i < 4);
			}
		}
		
		catch (SQLException e){
			System.out.println("Erreur liee a la recuperation des capacites en BDD !");
		}
		rs = null;
		return listeCapacitesMonstre;
	}
	
	public static int setCapacitesMonstre(int idMonstre, int idCapacite) {
		int id = 0;
	    try {	     
	    	String requete = "INSERT INTO monster_capacity(mst_cpt_monster_id, mst_cpt_capacity_id) VALUES("
	    			+ " '"+ idMonstre +"', '"+ idCapacite + "')";
	    	id = getInsertId(requete);
	    } catch (SQLException e) {
	        if(e.getErrorCode() == 1062) {
	            JOptionPane.showConfirmDialog(null, "La capacite existe deja !", "Dev Montser", 
	            		JOptionPane.PLAIN_MESSAGE);
	            return id;
	        }
	        e.printStackTrace();
	    }
	    return id;
	}
	
	public static boolean updateCapacitesMonstre(int id, int idMonstre, int idCapacite) {
		try {
			st.executeUpdate("UPDATE monster_capacity SET mst_cpt_monster_id = '" + idMonstre + "', mst_cpt_capacity_id = '" + idCapacite + 
					"' WHERE mst_cpt_id  = '" + id + "'");
		} 
		
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "La capacite du monstre n'a pas ete mise a jour", "Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean deleteCapacitesMonstre(int id) {
		try {
			st.executeUpdate("DELETE FROM monster_capacity WHERE mst_cpt_id = " + id);
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1062) {
				JOptionPane.showConfirmDialog(null, "La capacite du monstre n'a pas �t� supprim�e", 
						"Dev Montser", JOptionPane.PLAIN_MESSAGE);
				return false;
			}
			e.printStackTrace();
		}
		return true;
	}
		
	private static int getInsertId(String requete) throws SQLException {
		PreparedStatement statement = cn.prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
        statement.executeUpdate();
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getInt(1);   
	}
	
}
	/*public void test() {
		
		
		try {
			st = (Statement) cn.createStatement();
			ResultSet rs = st.executeQuery("select * from user") ;
			
			if(rs.next()) {
				System.out.println(rs.getInt(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4));
			}
		} catch (SQLException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
