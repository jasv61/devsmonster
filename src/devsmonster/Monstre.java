package devsmonster;

import java.util.HashMap;
import java.util.Map;

public class Monstre {

	private int id;
	private String nom;
	private int attaque;
	private int defense;
	private int vitesse;
	private int pv;
	private float coefExperience;
	private String image;
	private String imageDos;
	private int idType;
	public static Map<Integer, Monstre> listeMonstre = new HashMap<>();
	private int[] capacites = new int[4];

	public Monstre(int id, String nom, int attaque, int defense, int vitesse, int pv, float coefExperience, String image, String imageDos, int idType){
		this.id = id;
		this.nom = nom;
		this.attaque = attaque;
		this.defense = defense;
		this.vitesse = vitesse;
		this.pv = pv;
		this.coefExperience = coefExperience;
		this.image = image;
		this.imageDos = imageDos;
		this.idType = idType;
	}

	public int getId (){
		return this.id;
	}
	
	public String getNom() { 
		return this.nom;
	}

	public void setNom(String nom) { 
		this.nom = nom;
	}
	
	public int getBaseAttaque() { 
		return this.attaque;
	}
	
	public void setBaseAttaque(int attaque) { 
		this.attaque = attaque;
	}
	
	public int getBaseDefense() { 
		return this.defense;
	}
	
	public void setBaseDefense(int defense) { 
		this.defense = defense;
	}
	
	public int getBaseVitesse() { 
		return this.vitesse;
	}
	
	public void setBaseVitesse(int vitesse) { 
		this.vitesse = vitesse;
	}
	
	public int getBasePv() { 
		return this.pv;
	}
	
	public void setBasePv(int pv) { 
		this.pv = pv;
	}
	
	public float getCoefExperience() { 
		return this.coefExperience;
	}
	
	public void setCoefExperience(float coefExperience) { 
		this.coefExperience = coefExperience;
	}
	
	public String getImage(){ 
		return this.image;
	}
	
	public void setImage(String image){ 
		this.image = image;
	}
	
	public String getImageDos(){ 
		return this.imageDos;
	}
	
	public void setImageDos(String imageDos){ 
		this.imageDos = imageDos;
	}
	
	public int getIdType(){ 
		return this.idType;
	}
	
	public void setIdType(int idType){ 
		this.idType = idType;
	}
	
	public Type getType(){ 
		return Type.listeType.get(this.idType);
	}
	
	private int getStat(int base) {
		int stat = ((2 * base * this.getNiveau())/100)+5;
		return stat;
	}
	
	public int getNiveau() {
		int niv = 50;
		return niv;
	}
	
	public int getAttaque() {
		int att = this.getStat(this.attaque);
		return att;
	}
	
	public int getDefense() {
		int def = this.getStat(this.defense);
		return def;
	}
	
	public int getVitesse() {
		int spd = this.getStat(this.vitesse);
		return spd;
	}

	public int getPv() {
		int pv = this.getStat(this.pv)+this.getNiveau()+5;
		return pv;
	}
	
	public int[] getCapacites() {
		return capacites;
	}
	
	public Capacite getCapacite(int i) {
		return Capacite.listeCapacite.get(capacites[i]);
	}
	
	public void setCapacites(int[] capacites) {
		this.capacites = capacites;
	}
	
	public void updateBdd() {
		BDD.updateMonstre(id, nom, attaque, defense, vitesse, pv, coefExperience, "", idType);
	}
	
	public static void resume() {
		System.out.println("\nListe des monstres : ");
		for (Map.Entry<Integer, Monstre> monstre : Monstre.listeMonstre.entrySet()) {
			System.out.println("\n- id = " + monstre.getKey() + " / nom = " + monstre.getValue().getNom() + " / attaque = " + monstre.getValue().getAttaque() 
					+ " / defense = " + monstre.getValue().getDefense() + " / vitesse = " + monstre.getValue().getVitesse() + " / point de vie = " 
					+ monstre.getValue().getPv() + " / coefficient d'experience = " + monstre.getValue().getCoefExperience() + " / type = " 
					+ (String)Type.listeType.get(monstre.getValue().getIdType()).getNom());
		}
	}
}
