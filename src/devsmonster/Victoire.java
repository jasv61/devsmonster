package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class Victoire extends JPanel{

	public Utilisateur grandVainqueur;
	public Utilisateur grandPerdant;
	public Victoire(Utilisateur utilisateur,Utilisateur utilisateur2) {
		grandVainqueur = utilisateur;
		grandPerdant = utilisateur2;
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	public void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[20%][20%][20%][20%][20%]"));
		
		JLabel lblNewLabel = new JLabel("Félicitation " + grandVainqueur.getNom() + "!");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		add(lblNewLabel, "cell 0 1,alignx center,aligny center");
		
		JLabel lblNewLabel_1 = new JLabel("Tu as vaincu " + grandPerdant.getNom() + "!");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		add(lblNewLabel_1, "cell 0 2,alignx center,aligny center");
		
		JButton btnNewButton = new JButton("Quitter");
		add(btnNewButton, "cell 0 4,alignx center,aligny center");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectMode accueil = new SelectMode();
				Main.fen.setContentPane(accueil);
			}
		});
	}
	
}
