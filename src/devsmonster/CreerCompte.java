package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CreerCompte extends JPanel{
	private JTextField tfLogin;
	private JPasswordField tfPassword;
	
	public CreerCompte() {
		initialize();
	}
	
	public void initialize() {
		this.setBounds(12, 13, 769, 464);
		setLayout(new MigLayout("", "[50%][50%,grow]", "[25%][25%][25%][25%]"));
		
		JLabel lblInfo = new JLabel("Cr\u00E9ation de compte");
		add(lblInfo, "cell 0 0 2 1,alignx center");
		
		JLabel lblLogin = new JLabel("Login");
		add(lblLogin, "cell 0 1,alignx trailing");
		
		tfLogin = new JTextField();
		add(tfLogin, "cell 1 1,alignx left");
		tfLogin.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		add(lblMotDePasse, "cell 0 2,alignx trailing");
		
		tfPassword = new JPasswordField();
		add(tfPassword, "cell 1 2,alignx left");
		tfPassword.setColumns(10);
		
		JButton btnCreateAccount = new JButton("Cr\u00E9er");
		add(btnCreateAccount, "cell 0 3 2 1,alignx center,aligny center");
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tfLogin.getText().isEmpty() || String.copyValueOf(tfPassword.getPassword()).isEmpty()) {
					JOptionPane.showConfirmDialog(null,
							"Vous devez saisir un login et un mot de passe !",
						       "DevMonster",
							 JOptionPane.PLAIN_MESSAGE);
			
		}
		else {
				if(BDD.setUtilisateur(tfLogin.getText(), String.copyValueOf(tfPassword.getPassword())) != 0) {
					ConnexionUtilisateur connexionUtilisateur = new ConnexionUtilisateur();
					Main.fen.setContentPane(connexionUtilisateur);
					}
				}
			}
		});
	}

}
