package devsmonster;

import java.util.ArrayList;
import java.util.Map;

public class Utilisateur {
	public static Utilisateur utilisateur1;
	public static Utilisateur utilisateur2;
	private	int id;
	private	String nom;
	private String motDePasse;
	private ArrayList<MonstreUtilisateur> equipeMonstre = new ArrayList<MonstreUtilisateur>();
	private int idMonstreActuel = 0;
	
	public Utilisateur(int id, String nom, String motDePasse) {
		this.id = id;
		this.nom = nom;
		this.motDePasse = motDePasse;
	}
	
	public Utilisateur(int id, String nom, String motDePasse, ArrayList<MonstreUtilisateur> equipeMonstre) {
		this.id = id;
		this.nom = nom;
		this.motDePasse = motDePasse;
		this.equipeMonstre = equipeMonstre;
		this.resume();
	}

	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getMotDePasse() {
		return motDePasse;
	}
	
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	public ArrayList<MonstreUtilisateur> getEquipeMonstre() {
		return equipeMonstre;
	}
	
	public void setEquipeMonstre(ArrayList<MonstreUtilisateur> equipeMonstre) {
		this.equipeMonstre = equipeMonstre;
	}
	
	public int getIdMonstreActuel() {
		return idMonstreActuel;
	}
	
	public void setIdMonstreActuel(int idMonstreActuel) { 
		this.idMonstreActuel = idMonstreActuel;
	}
	
	public MonstreUtilisateur getMonstreActuel() {
		return equipeMonstre.get(idMonstreActuel);
	}
	
	public void updateBDD() {
		BDD.updateUtilisateur(id, nom, motDePasse);
	}
	
	public static void reset(Utilisateur utilisateur) {
		for (int i = 0; i < utilisateur.equipeMonstre.size(); i++) {
			MonstreUtilisateur monstre =  utilisateur.equipeMonstre.get(i);
			monstre.setPv(monstre.getMonster().getPv());
		}
		utilisateur.setIdMonstreActuel(0);
	}
	
	public void resume() {
		System.out.println("\nR�sum� utilisateur : ");
		
		System.out.println("id = " + id + " / nom = " + nom);
		
		System.out.println("- Liste des monstres : ");
		for (int i = 0; i < this.equipeMonstre.size(); i++) {
			MonstreUtilisateur monstre =  this.equipeMonstre.get(i);
			System.out.println(Monstre.listeMonstre.get(1).getNom());
			System.out.println("\n- id = " + monstre.getIdMonstreUtilisateur() + " / nom = " + monstre.getNom());
		}
	}
}
