package devsmonster;

public class Affinite {
	private int id;
	private int idType1;
	private int idType2;
	private float coef;
	
	public Affinite(int id, int idType1, int idType2, float coef) {
		this.id = id;
		this.idType1 = idType1;
		this.idType2 = idType2;
		this.coef = coef;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdType1() {
		return idType1;
	}
	
	public void setIdType1(int idType1) {
		this.idType1 = idType1;
	} 
	
	public int getIdType2() {
		return idType2;
	}
	
	public void setIdType2(int idType2) {
		this.idType2 = idType2;
	} 
	
	public float getCoef() {
		return coef;
	}
	
	public void setCoef(int coef) {
		this.coef = coef;
	} 
	
	public void updateAffiniteBdd() {
		BDD.updateAffinite(id, idType1, idType2, coef);
	}
}
