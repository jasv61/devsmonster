package devsmonster;

import java.util.HashMap;
import java.util.Map;


public class Main {
	public static Fenetre fen;
	
	public static void main(String[] args) {
		BDD.initialisationBDD();
		Main.initialisationJeux();
		Main.resumeJeux();
		fen = new Fenetre();
	}
	
	public static void initialisationJeux() {
		Map<Integer, devsmonster.Type> types = BDD.getTypes();
		
		for (Map.Entry<Integer, devsmonster.Type> type : types.entrySet()) {
			Map<Integer, Affinite> affinites = BDD.getAffinites(type.getKey());
			type.getValue().setListeAffinites(affinites);
		}
		
		devsmonster.Type.listeType = types;
		
		Capacite.listeCapacite = BDD.getCapacites();
		
		Monstre.listeMonstre = BDD.getMonstres();
		
		for(Map.Entry<Integer, Monstre> entry : Monstre.listeMonstre.entrySet()) {
			Monstre monstre = entry.getValue();
			monstre.setCapacites(BDD.getCapacitesMonstre(monstre.getId()));
		}
		
	}
	public static void resumeJeux() {
		System.out.println("\nDebut du r�sum� : ");
		devsmonster.Type.resume();
		Capacite.resume();
		Monstre.resume();
		System.out.println("Fin du r�sum�");
	}	
}
