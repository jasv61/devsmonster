package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import java.awt.Color;

public class SelectModeCreation extends JPanel{

	public SelectModeCreation() {
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	public void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[25%][25%][25%][25%][25%]"));
		
		JLabel LblSelectMode = new JLabel("Bonjour " + Utilisateur.utilisateur1.getNom() + ", que veux tu faire?");
		LblSelectMode.setForeground(Color.WHITE);
		add(LblSelectMode, "cell 0 0,alignx center");
		
		JButton btnCreateMonster = new JButton("Cr�ation de cr�ature");
		btnCreateMonster.setForeground(Color.WHITE);
		btnCreateMonster.setBackground(Color.GRAY);
		btnCreateMonster.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		add(btnCreateMonster, "cell 0 1,alignx center");
		
		JButton btnCreateType = new JButton("Cr�ation de types");
		btnCreateType.setForeground(Color.WHITE);
		btnCreateType.setBackground(Color.GRAY);
		add(btnCreateType, "cell 0 2,alignx center");
		
		btnCreateType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModePlayer selectModePlayer = new SelectModePlayer();
				Main.fen.setContentPane(selectModePlayer);
			}
		});
		
		JButton btnCreateCapacity = new JButton("Cr�ation de capacit�");
		btnCreateCapacity.setForeground(Color.WHITE);
		btnCreateCapacity.setBackground(Color.GRAY);
		add(btnCreateCapacity, "cell 0 3,alignx center,aligny center");
		btnCreateCapacity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormulaireCapacite formulaireCapacite = new FormulaireCapacite();
				Main.fen.setContentPane(formulaireCapacite);
			}
		});
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.setForeground(Color.WHITE);
		btnRetour.setBackground(Color.GRAY);
		add(btnRetour, "cell 0 4,alignx center");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectMode selectMode = new SelectMode();
				Main.fen.setContentPane(selectMode);
			}
		});
	}
	
}