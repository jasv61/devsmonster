package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import java.awt.Color;

public class SelectModePlayer extends JPanel {

	public SelectModePlayer() {
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	private void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[20%][20%][20%][20%][20%]"));
		JButton btnModeLocal = new JButton("Mode JcJ local");
		btnModeLocal.setBackground(Color.GRAY);
		btnModeLocal.setForeground(Color.WHITE);
		btnModeLocal.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) { 
				ConnexionUtilisateur2 connexionUtilisateur2 = new ConnexionUtilisateur2();
				Main.fen.setContentPane(connexionUtilisateur2);				
			} 
		});
		add(btnModeLocal, "flowx,cell 0 1,alignx center,aligny center"); 
		 
		JLabel lblVeuillezSlectionnerVotre = new JLabel("Veuillez s\u00E9lectionner votre mode de jeu :");
		lblVeuillezSlectionnerVotre.setForeground(Color.WHITE);
		add(lblVeuillezSlectionnerVotre, "cell 0 0,alignx center,growy"); 
		
		JButton btnModeJcia = new JButton("Mode JcIA");
		btnModeJcia.setForeground(Color.WHITE);
		btnModeJcia.setBackground(Color.GRAY);
		add(btnModeJcia, "cell 0 2,alignx center,aligny baseline"); 
		
		JButton btnModeJcjOnline = new JButton("Mode JcJ Online");
		btnModeJcjOnline.setBackground(Color.GRAY);
		btnModeJcjOnline.setForeground(Color.WHITE);
		add(btnModeJcjOnline, "cell 0 3,alignx center"); 
		
		JButton btnRetourSelectionMode = new JButton("Retour");
		btnRetourSelectionMode.setForeground(Color.WHITE);
		btnRetourSelectionMode.setBackground(Color.GRAY);
		btnRetourSelectionMode.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) { 
				SelectMode selectMode = new SelectMode();
				Main.fen.setContentPane(selectMode);				
			} 
		});
		add(btnRetourSelectionMode, "cell 0 4,alignx center,aligny center"); 
	}
}
