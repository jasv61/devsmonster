package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import java.awt.Color;

public class FormulaireCapacite extends JPanel{
	private JTextField textFieldNom;

	public FormulaireCapacite() {
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	public void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[50%,grow][50%,grow]", "[20%][20%][20%][20%,grow][20%]"));
		
		JLabel lblInfo = new JLabel("Cr\u00E9ation des capacit\u00E9s");
		lblInfo.setForeground(Color.WHITE);
		add(lblInfo, "cell 0 0 2 1,alignx center,aligny center");
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setForeground(Color.WHITE);
		add(lblNom, "cell 0 1,alignx trailing,aligny center");
		
		textFieldNom = new JTextField();
		add(textFieldNom, "cell 1 1,alignx left,aligny center");
		textFieldNom.setColumns(10);
		
		JLabel lblDegats = new JLabel("D\u00E9gats");
		lblDegats.setForeground(Color.WHITE);
		add(lblDegats, "cell 0 2,alignx trailing,aligny center");
		
		JLabel lblType = new JLabel("Type");
		lblType.setForeground(Color.WHITE);
		add(lblType, "cell 0 3,alignx trailing");
		
		JComboBox comboBoxType = new JComboBox();
		add(comboBoxType, "cell 1 3,alignx left,aligny center");
		
		for (Map.Entry<Integer, Type> type : Type.listeType.entrySet()) {
			comboBoxType.addItem(type.getValue());
		}
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.setBackground(Color.GRAY);
		btnRetour.setForeground(Color.WHITE);
		add(btnRetour, "cell 1 4");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModeCreation selectModeCreation = new SelectModeCreation();
				Main.fen.setContentPane(selectModeCreation);
			}
		});
		
		JSlider sliderDegats = new JSlider();
		add(sliderDegats, "flowx,cell 1 2");
		sliderDegats.setMaximum(100);
		sliderDegats.setMinimum(0);
		sliderDegats.setValue(0);
		
		JLabel lblNbDegats = new JLabel("0");
		lblNbDegats.setForeground(Color.WHITE);
		add(lblNbDegats, "cell 1 2");
		sliderDegats.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent event){
				lblNbDegats.setText("" + ((JSlider)event.getSource()).getValue());
		      }
		});
		
		JButton btnValider = new JButton("Valider");
		btnValider.setForeground(Color.WHITE);
		btnValider.setBackground(Color.GRAY);
		add(btnValider, "cell 0 4,alignx right,aligny center");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nom = textFieldNom.getText();
				int degats = sliderDegats.getValue();
				String animation = "";
				Type type = (Type)comboBoxType.getSelectedItem();
				
				if(BDD.setCapacite(nom, degats, animation, type.getId()) != 0) {
					Main.initialisationJeux();
					SelectModeCreation selectModeCreation = new SelectModeCreation();
					Main.fen.setContentPane(selectModeCreation);
					}
				}
		});
	}
}