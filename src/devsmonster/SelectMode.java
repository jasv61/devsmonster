package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Color;

public class SelectMode extends JPanel{

	public SelectMode() {
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	public void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[25%][25%][25%][25%][25%]"));
		
		JLabel LblSelectMode = new JLabel("Bonjour " + Utilisateur.utilisateur1.getNom() + ", que veux tu faire?");
		LblSelectMode.setForeground(Color.WHITE);
		add(LblSelectMode, "cell 0 0,alignx center");
		
		JButton btnCreateMonster = new JButton("Cr�ation de cr�ature / types / capacit�");
		btnCreateMonster.setForeground(Color.WHITE);
		btnCreateMonster.setBackground(Color.GRAY);
		btnCreateMonster.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		add(btnCreateMonster, "cell 0 1,alignx center");
		btnCreateMonster.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModeCreation selectModeCreation = new SelectModeCreation();
				Main.fen.setContentPane(selectModeCreation);
			}
		});
		
		
		JButton btnModeJoueur = new JButton("Mode JCJ Local / JCA / JCJ Online");
		btnModeJoueur.setBackground(Color.GRAY);
		btnModeJoueur.setForeground(Color.WHITE);
		add(btnModeJoueur, "cell 0 2,alignx center");
		btnModeJoueur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModePlayer selectModePlayer = new SelectModePlayer();
				Main.fen.setContentPane(selectModePlayer);
			}
		});
		
		JButton btnModeHistoire = new JButton("Mode histoire / Solo");
		btnModeHistoire.setForeground(Color.WHITE);
		btnModeHistoire.setBackground(Color.GRAY);
		add(btnModeHistoire, "cell 0 3,alignx center,aligny center");
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.setBackground(Color.GRAY);
		btnRetour.setForeground(Color.WHITE);
		add(btnRetour, "cell 0 4,alignx center");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConnexionUtilisateur connexionUtilisateur = new ConnexionUtilisateur();
				Main.fen.setContentPane(connexionUtilisateur);
			}
		});
	}
	
}
