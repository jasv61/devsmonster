package devsmonster;

public class Battle {
	
	private boolean tour = false;
	private boolean fin = false;
	
	public Battle() {
		Utilisateur.reset(Utilisateur.utilisateur1);
		Utilisateur.reset(Utilisateur.utilisateur2);
	}
	
	public boolean getTour() {
		return tour;
	}
	
	public void setTour(boolean tour) {
		this.tour = tour;
	}
	
	public boolean getFin() {
		return fin;
	}
	
	public void setFin(boolean fin) {
		this.fin = fin;
	}
	
	public int attaque(MonstreUtilisateur monstre1, MonstreUtilisateur monstre2, int idAttaque) {
		float coef = getCoef(monstre1, monstre2, idAttaque);
		int niv = monstre1.getNiveau();
		int att = monstre1.getAttaque();
		int domm = monstre1.getMonster().getCapacite(idAttaque).getDommage();
		int def = monstre2.getDefense();
		int pvPerdu = (int) (((((niv * 0.4 + 2) * att * domm)/(def * 50))+2)*coef);
		return pvPerdu;
	}
	
	public float getCoef(MonstreUtilisateur monstre1, MonstreUtilisateur monstre2, int idAttaque) {
		int idTypeAttaque = monstre1.getMonster().getCapacite(idAttaque).getIdType();
		if(monstre2.getMonster().getType().getListeAffinites().containsKey(idTypeAttaque)) {
			return monstre2.getMonster().getType().getListeAffinites().get(idTypeAttaque).getCoef();
		}
		return (float) 1;
	}
}
