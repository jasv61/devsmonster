package devsmonster;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Accueil extends JPanel{

	public Accueil() {
		initialize();
	}
	
	public void initialize() {
		this.setBounds(12, 13, 769, 464);
		setLayout(new MigLayout("", "[100%]", "[100%]"));
		
		JButton btnNewButton = new JButton("Appuyer ici");
		add(btnNewButton, "cell 0 0,alignx center,aligny center");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConnexionUtilisateur connexionUtilisateur = new ConnexionUtilisateur();
				Main.fen.setContentPane(connexionUtilisateur);
			}
		});
	}
	
}
