package devsmonster;

import java.util.HashMap;
import java.util.Map;

public class Capacite {
	private int id;
	private String nom;
	private int dommage;
	private String animation;
	private int idType;
	public static Map<Integer, Capacite> listeCapacite = new HashMap<>();

	public Capacite(int id, String nom, int dommage, String animation, int idType){
		this.id = id;
		this.nom = nom;
		this.dommage = dommage;
		this.animation = animation;
		this.idType = idType;
	}
	
	public int getId (){
		return this.id; 
	}
	
	public String getNom() { 
		return this.nom; 
	}
	
	public int getDommage() { 
		return this.dommage; 
	}
	
	public String getAnimation() { 
		return this.animation; 
	}
	
	public int getIdType() { 
		return idType;
	}
	
	public Type getType(){ 
		return Type.listeType.get(this.idType);
	}
	
	public static void resume() {
		System.out.println("\nListe des capacitÚs : ");
		for (Map.Entry<Integer, Capacite> capacite : Capacite.listeCapacite.entrySet()) {
			System.out.println("\n- id = " + capacite.getKey() + " / nom = " + capacite.getValue().getNom() + " / dommage = " + capacite.getValue().getDommage()
					+ " / type = " + (String)Type.listeType.get(capacite.getValue().getIdType()).getNom());
		}
	}
}