package devsmonster;

import javax.swing.JFrame;

public class Fenetre extends JFrame {
  public Fenetre(){
    this.setTitle("Dev's Monster");
    this.setSize(800, 500);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);          
    ConnexionUtilisateur accueil = new ConnexionUtilisateur();
    this.setContentPane(accueil);
    this.setVisible(true);
  }
}
