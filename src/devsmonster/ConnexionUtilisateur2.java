package devsmonster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;

public class ConnexionUtilisateur2 extends JPanel{
	private JTextField textUtilisateur;
	private JPasswordField passwordConnect;
	
	public ConnexionUtilisateur2() {
		setBackground(Color.DARK_GRAY);
		initialize();
	}
	
	public void initialize() {
		this.setBounds(0, 0, 769, 464);
		setLayout(new MigLayout("", "[50%][50%,grow]", "[20%][20%][20%][20%][20%]"));
		
		JLabel lblTitle = new JLabel("Connexion joueur 2");
		lblTitle.setForeground(Color.WHITE);
		add(lblTitle, "cell 0 0 2 1,alignx center,aligny center");
		
		JLabel LblNomUtilisateur = new JLabel("Nom d'utilisateur");
		LblNomUtilisateur.setForeground(Color.WHITE);
		add(LblNomUtilisateur, "cell 0 1,alignx trailing");
		
		textUtilisateur = new JTextField();
		add(textUtilisateur, "cell 1 1,alignx left,aligny center");
		textUtilisateur.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setForeground(Color.WHITE);
		add(lblMotDePasse, "cell 0 2,alignx trailing");
		
		passwordConnect = new JPasswordField();
		add(passwordConnect, "flowx,cell 1 2,alignx left");
		passwordConnect.setColumns(10);
		
		JButton btnConnexion = new JButton("Connexion");
		btnConnexion.setBackground(Color.GRAY);
		btnConnexion.setForeground(Color.WHITE);
		add(btnConnexion, "flowx,cell 0 3 2 1,alignx center,aligny center");
		
		JLabel LblInfo = new JLabel("");
		LblInfo.setForeground(Color.RED);
		add(LblInfo, "flowx,cell 0 4 2 1,alignx center");
		
		btnConnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nomUtilisateur = textUtilisateur.getText();
				String motDePasseUtilisateur = String.valueOf(passwordConnect.getPassword());
				int idUtilisateur = BDD.testUtilisateur(nomUtilisateur, motDePasseUtilisateur);
				
				if(idUtilisateur != 0) {
					Utilisateur.utilisateur2 = new Utilisateur(idUtilisateur, nomUtilisateur, motDePasseUtilisateur, BDD.getMonstresUtilisateur(idUtilisateur));
					ViewBattle viewBattle = new ViewBattle();
					Main.fen.setContentPane(viewBattle);
				} else {
					if(textUtilisateur.getText().isEmpty()) {
						LblInfo.setText("Vous devez saisir un utilisateur");
						LblInfo.setVisible(true);
						textUtilisateur.requestFocus();
					}
					if(String.copyValueOf(passwordConnect.getPassword()).isEmpty()) {
						LblInfo.setText("Vous devez saisir un mot de passe");
						LblInfo.setVisible(true);
						passwordConnect.requestFocus();
					}
					else {
					LblInfo.setText("Utilisateur Inconnu"); 
					LblInfo.setVisible(true);
					textUtilisateur.requestFocus();
					}
				}	
			}	
	});
		
		JButton btnQuitter = new JButton("Retour");
		btnQuitter.setBackground(Color.GRAY);
		btnQuitter.setForeground(Color.WHITE);
		add(btnQuitter, "cell 0 4,alignx center,aligny center");
		
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectModePlayer selectModePlayer = new SelectModePlayer();
				Main.fen.setContentPane(selectModePlayer);
			}
		});		
	}
}
