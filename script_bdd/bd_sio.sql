-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 07 mai 2020 à 00:04
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bd_sio`
--

-- --------------------------------------------------------

--
-- Structure de la table `affinity`
--

CREATE TABLE `affinity` (
  `aff_id` int(11) NOT NULL,
  `aff_coef` float NOT NULL,
  `aff_tpe_for_id` int(11) NOT NULL,
  `aff_tpe_against_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `capacity`
--

CREATE TABLE `capacity` (
  `cpt_id` int(11) NOT NULL,
  `cpt_name` varchar(255) NOT NULL,
  `cpt_damage` int(11) NOT NULL,
  `cpt_animation` varchar(255) NOT NULL,
  `cpt_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `monster`
--

CREATE TABLE `monster` (
  `mst_id` int(11) NOT NULL,
  `mst_name` varchar(255) NOT NULL,
  `mst_attack` int(11) NOT NULL,
  `mst_defense` int(11) NOT NULL,
  `mst_speed` int(11) NOT NULL,
  `mst_life_point` int(11) NOT NULL,
  `mst_experience_coef` float NOT NULL,
  `mst_image` varchar(255) NOT NULL,
  `mst_image_back` varchar(255) NOT NULL,
  `mst_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `monster_capacity`
--

CREATE TABLE `monster_capacity` (
  `mst_cpt_id` int(11) NOT NULL,
  `mst_cpt_monster_id` int(11) NOT NULL,
  `mst_cpt_capacity_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `tpe_id` int(11) NOT NULL,
  `tpe_nom` varchar(255) NOT NULL,
  `tpe_color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `usr_id` int(11) NOT NULL,
  `usr_login` varchar(255) NOT NULL,
  `usr_password` varchar(255) NOT NULL,
  `usr_image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_monster`
--

CREATE TABLE `user_monster` (
  `usr_mst_id` int(11) NOT NULL,
  `usr_mst_name` varchar(255) NOT NULL,
  `usr_mst_experience` int(11) NOT NULL,
  `usr_mst_date_catch` date NOT NULL,
  `usr_mst_user_id` int(11) NOT NULL,
  `usr_mst_monster_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affinity`
--
ALTER TABLE `affinity`
  ADD PRIMARY KEY (`aff_id`),
  ADD KEY `aff_tpe_against_id` (`aff_tpe_against_id`),
  ADD KEY `aff_tpe_for_id` (`aff_tpe_for_id`);

--
-- Index pour la table `capacity`
--
ALTER TABLE `capacity`
  ADD PRIMARY KEY (`cpt_id`),
  ADD KEY `cpt_type_id` (`cpt_type_id`);

--
-- Index pour la table `monster`
--
ALTER TABLE `monster`
  ADD PRIMARY KEY (`mst_id`),
  ADD KEY `mst_type_id` (`mst_type_id`);

--
-- Index pour la table `monster_capacity`
--
ALTER TABLE `monster_capacity`
  ADD PRIMARY KEY (`mst_cpt_id`),
  ADD KEY `mst_cpt_capacity_id` (`mst_cpt_capacity_id`),
  ADD KEY `mst_cpt_monster_id` (`mst_cpt_monster_id`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`tpe_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`),
  ADD UNIQUE KEY `usr_login` (`usr_login`);

--
-- Index pour la table `user_monster`
--
ALTER TABLE `user_monster`
  ADD PRIMARY KEY (`usr_mst_id`),
  ADD KEY `usr_mst_user_id` (`usr_mst_user_id`),
  ADD KEY `usr_mst_monster_id` (`usr_mst_monster_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `affinity`
--
ALTER TABLE `affinity`
  MODIFY `aff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `capacity`
--
ALTER TABLE `capacity`
  MODIFY `cpt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `monster`
--
ALTER TABLE `monster`
  MODIFY `mst_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `monster_capacity`
--
ALTER TABLE `monster_capacity`
  MODIFY `mst_cpt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `tpe_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user_monster`
--
ALTER TABLE `user_monster`
  MODIFY `usr_mst_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `affinity`
--
ALTER TABLE `affinity`
  ADD CONSTRAINT `affinity_ibfk_1` FOREIGN KEY (`aff_tpe_against_id`) REFERENCES `type` (`tpe_id`),
  ADD CONSTRAINT `affinity_ibfk_2` FOREIGN KEY (`aff_tpe_for_id`) REFERENCES `type` (`tpe_id`);

--
-- Contraintes pour la table `capacity`
--
ALTER TABLE `capacity`
  ADD CONSTRAINT `capacity_ibfk_1` FOREIGN KEY (`cpt_type_id`) REFERENCES `type` (`tpe_id`);

--
-- Contraintes pour la table `monster`
--
ALTER TABLE `monster`
  ADD CONSTRAINT `monster_ibfk_1` FOREIGN KEY (`mst_type_id`) REFERENCES `type` (`tpe_id`);

--
-- Contraintes pour la table `monster_capacity`
--
ALTER TABLE `monster_capacity`
  ADD CONSTRAINT `monster_capacity_ibfk_1` FOREIGN KEY (`mst_cpt_capacity_id`) REFERENCES `capacity` (`cpt_id`),
  ADD CONSTRAINT `monster_capacity_ibfk_2` FOREIGN KEY (`mst_cpt_monster_id`) REFERENCES `monster` (`mst_id`);

--
-- Contraintes pour la table `user_monster`
--
ALTER TABLE `user_monster`
  ADD CONSTRAINT `user_monster_ibfk_1` FOREIGN KEY (`usr_mst_user_id`) REFERENCES `user` (`usr_id`),
  ADD CONSTRAINT `user_monster_ibfk_2` FOREIGN KEY (`usr_mst_monster_id`) REFERENCES `monster` (`mst_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
